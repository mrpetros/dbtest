package com.karpinski.dbtest;

import com.karpinski.dbtest.model.Device;
import com.karpinski.dbtest.model.DeviceRepository;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;


import java.util.List;

@RunWith(SpringRunner.class)
@SpringBootTest
public class BasicTests {

    @Autowired
    DeviceRepository deviceRepository;

    @Before
    public void doBefore() {
        System.out.println("AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA");
    }

    @Test
    public void readTest() {
        List<Device> devices = deviceRepository.findAll();
        assertEquals(devices.size(), 5);
    }


}

package com.karpinski.dbtest.model;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.List;


public interface JoinRepository extends CrudRepository<Device, Long> {

    List<Device> findAll();
    Device findTopByOrderByIdDesc();
}

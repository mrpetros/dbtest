package com.karpinski.dbtest.model;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.ToString;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Table(name = "device")


@Entity
@Data
@AllArgsConstructor
@ToString
@Builder

public class Device {

    public Device() {

    }

    @Id
    @GeneratedValue
    @Column(name = "id")
    private Long id;

    @Column(name = "mac")
    private String mac;

    @OneToMany
    @JoinTable(name = "neighbours", joinColumns = @JoinColumn(name="n_id"), inverseJoinColumns = @JoinColumn(name = "id"))
    @JsonIgnoreProperties("neighbours")
    private List<Device> neighbours = new ArrayList<>();

}

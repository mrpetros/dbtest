package com.karpinski.dbtest.controllers;

import com.karpinski.dbtest.model.Device;
import com.karpinski.dbtest.model.JoinRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping("/db")
public class DbTestController {

    @Autowired
    JoinRepository joinRepository;

    @GetMapping("/showtable")
    public ResponseEntity<List<Device>> showTable() {
        List<Device> allDevs = joinRepository.findAll();
        System.out.println("Wejscie do kontrollera");
        allDevs.forEach(device -> {
            System.out.println("Device = " + device.getId() + " " + device.getMac());
        });
        System.out.println("Wyjscie z kontrollera");
        ResponseEntity response = ResponseEntity.status(HttpStatus.OK).body(allDevs);
//        printReferences(allDevs);
        Device dev = joinRepository.findTopByOrderByIdDesc();
        System.out.println("Najwiekszy id = " + dev.getId() + " " + dev.getMac());
        return response;
    }

    private void printReferences(List<Device> allDevs) {
        allDevs.forEach(device -> {
            System.out.println(device);
            System.out.println(device.hashCode());
            System.out.println("================");
        });


    }

    @PutMapping("/adddevice")
    public void addDevice() {
        System.out.println("ADDDDDDDD DEVICE");
        Device newDevice = new Device();
        newDevice.setMac("AA-BB-CC-DD");
        List<Device> list = joinRepository.findAll();
        createCycle(newDevice, list);
        newDevice.setNeighbours(list);
        joinRepository.save(newDevice);
    }

    private void createCycle(Device newDevice, List<Device> list) {
        list.get(0).getNeighbours().add(newDevice);
    }



    @DeleteMapping("/deleteDevice")
    public void deleteDevice(@RequestParam Long devId) {

        joinRepository.deleteById(devId);

    }

    @GetMapping("/neighbours")
    public ResponseEntity<List<Device>> findNeighbours(@RequestParam Long devId) {
        List<Device> allDevs = joinRepository.findAll();
        System.out.println("Wejscie do kontrollera");
        Device dev = allDevs.stream().filter(device -> device.getId()==devId).findFirst().get();
        List<Device> neighbours = dev.getNeighbours();
        neighbours.forEach(device -> {
            System.out.println("Device = " + device.getId() + " " + device.getMac());
        });

        System.out.println("Wyjscie z kontrollera");
        ResponseEntity response = ResponseEntity.status(HttpStatus.OK).body(allDevs);
        return response;
    }

}

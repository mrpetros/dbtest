﻿CREATE TABLE device 
(
	id BIGINT NOT NULL,
	mac varchar(255)
	--PRIMARY KEY (id)
);

INSERT INTO device (id, mac) VALUES (1, 'CD-ED-FG-CC');
INSERT INTO device (id, mac) VALUES (2, 'CD-ED-FG-DD');
INSERT INTO device (id, mac) VALUES (3, 'CD-ED-FG-EE');
INSERT INTO device (id, mac) VALUES (4, 'CD-ED-FG-FF');


SELECT * FROM device;

DROP TABLE device



-----sequence
CREATE SEQUENCE hibernate_sequence
    START WITH 1
    INCREMENT BY 1
    NO MAXVALUE
    NO MINVALUE
    CACHE 1;


---- JOIN TABLE
CREATE TABLE neighbours 
(
	id BIGINT,
	n_id BIGINT
)

SELECT * FROM neighbours 

DROP TABLE device